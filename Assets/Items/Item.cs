using UnityEngine;

public abstract class Item : ScriptableObject
{

}

public interface iItemData<T> {
    public string Name { get; }
    public string ConceptName { get; }
    public string Description { get; }
    public T getTypeItem();
 }
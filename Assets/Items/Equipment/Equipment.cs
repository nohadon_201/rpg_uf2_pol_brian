using UnityEngine;
using System.Collections.Generic;
public abstract class Equipment : Item
{
    abstract public List<Object> GetComponents();

}

public enum Element
{
    NONE,
    Fire,
    Ice,
    Light,
    Shadow,
    Wind,
    Earth
}

public enum SlotsArmor
{
    Shoulders,
    Chest,
    Belt,
    Helmet,
    Hands,
    Boots,
    Pants,
    Cloak
}

public enum SlotsJewels
{
    Neck,
    Ring0,
    Ring1,
    L_Wrist,
    R_Wrist,
}

public enum SlotsWeapons
{
    LeftHand,
    RightHand
}

public enum WeaponType
{
    Sword, Great_Sword, Axe, Great_Axe, Hammer, War_Hammer, Dagger, Halberds
}

public enum ArmorType { 
    Cloth, Leather, Mail, Plate
}

public enum JewelType {
    Ring, Necklace, Earring, Wrist
}


using System.Collections.Generic;
using UnityEngine;

public class Defensive : EquipmentDecorator
{
    [SerializeField] int m_Val;
    [SerializeField] Element m_Element;
    [SerializeField] int m_ValElement;
    public int Val { get => m_Val; }
    public Element Element { get => m_Element; }
    public int ValElement { get => m_ValElement; }
    public Defensive(Equipment equipment, int val, Element ele, int valEle) : base(equipment)
    {
        m_Val = val;
        m_Element = ele;
        m_ValElement = valEle;
    }

    public override List<Object> GetComponents()
    {
        List<Object> components = base.GetComponents();
        components.Add(this);
        return components;
    }

    public override string ToString()
    {
        return $"Type Defensive \n Value: {m_Val} \n Element: {m_Element} \n Element value: {m_ValElement}";
    }

}

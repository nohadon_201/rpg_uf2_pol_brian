
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Jewel", menuName = "Item/Equipment/New Jewel", order = 1)]
public class Jewel : BaseEquipment<SlotsJewels>, iItemData<Jewel>
{
    public Jewel(SlotsJewels slotTofill, List<SlotsJewels> slotsWichOccup, List<Classes> compatibleWith, MyAttributes attr, JewelType jwlType)
    {
        m_SlotToFill = slotTofill;
        m_SlotsWichOccup = slotsWichOccup;
        m_CompatibleWith = compatibleWith;
        m_Attributes = attr;
        m_JewelType = jwlType;
    }

    [SerializeField] JewelType m_JewelType;
    public JewelType JewelType { get => m_JewelType; }
    public string Name => m_ItemName;
    public string Description => m_Description;
    public string ConceptName => m_ConceptTypeItem;
    public override List<Object> GetComponents()
    {
        List<Object> components = new List<Object>();
        components.Add(this);
        return components;
    }

    public override string ToString()
    {
        return base.ToString() + $"Type Jewel: {m_JewelType} \n Slot fill: {m_SlotToFill} \n Slot which occup: {m_SlotsWichOccup} \n Compatible classes: {m_CompatibleWith} \n {m_Attributes}";
    }

    public Jewel getTypeItem()
    {
        return this;
    }
}
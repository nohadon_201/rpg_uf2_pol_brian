
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "Item/Equipment/New Weapon", order = 1)]
public class Weapon : BaseEquipment<SlotsWeapons>, iItemData<Weapon>
{
    public Weapon(SlotsWeapons slotTofill, List<SlotsWeapons> slotsWichOccup, List<Classes> compatibleWith , MyAttributes attr, WeaponType wpType) {
        m_SlotToFill = slotTofill;
        m_SlotsWichOccup = slotsWichOccup;
        m_CompatibleWith = compatibleWith;
        m_Attributes = attr;
        m_WeaponType = wpType;
    }

    [SerializeField] WeaponType m_WeaponType;
    public WeaponType WeaponType { get => m_WeaponType; }

    public string Name => m_ItemName;
    public string Description => m_Description;
    public string ConceptName => m_ConceptTypeItem;

    public override List<Object> GetComponents()
    {
        List<Object> components = new List<Object>();
        components.Add(this);
        return components;
    }

    public override string ToString()
    {
        return base.ToString() + $"Type Weapon: {m_WeaponType} \n Slot fill: {m_SlotToFill} \n Slot which occup: {m_SlotsWichOccup} \n Compatible classes: {m_CompatibleWith} \n {m_Attributes}";
    }

    public Weapon getTypeItem()
    {
        return this;
    }


}

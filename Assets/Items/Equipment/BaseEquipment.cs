
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseEquipment<T> : Equipment
{

    [SerializeField] protected string m_ItemName;
    [SerializeField] protected string m_ConceptTypeItem;
    [SerializeField, TextArea] protected string m_Description;
    [SerializeField] protected T m_SlotToFill;
    [SerializeField] protected List<T> m_SlotsWichOccup;
    [SerializeField] protected List<Classes> m_CompatibleWith;
    [SerializeField] protected MyAttributes m_Attributes;
    public T SlotToFill { get => m_SlotToFill; }
    public List<T> SlotsWichOccup { get => m_SlotsWichOccup; }
    public List<Classes> CompatibleWith { get => m_CompatibleWith; }
    public MyAttributes Attributes { get => m_Attributes; }
    public abstract override List<Object> GetComponents();
    public void setInfoItem(string itemName, string conceptTypeItem, string description)
    {
        m_ItemName = itemName;
        m_ConceptTypeItem = conceptTypeItem;
        m_Description = description;
    }

    public override string ToString()
    {
        return $"Name item: {m_ItemName} \n Concept type item: {m_ConceptTypeItem} \n Description: {m_Description} \n";
    }
}
using UnityEngine;
using System.Collections.Generic;
public abstract class EquipmentDecorator : Equipment
{
    private Equipment m_Equipment;
    public EquipmentDecorator(Equipment equipment)
    {
        m_Equipment = equipment;
    }

    public override List<Object> GetComponents()
    {
        return m_Equipment.GetComponents();
    }
}

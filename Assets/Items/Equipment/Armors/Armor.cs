
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Armor", menuName = "Item/Equipment/New Armor", order = 1)]
public class Armor : BaseEquipment<SlotsArmor>, iItemData<Armor>
{
    public Armor(SlotsArmor slotTofill, List<SlotsArmor> slotsWichOccup, List<Classes> compatibleWith, MyAttributes attr, ArmorType arType)
    {

        m_SlotToFill = slotTofill;
        m_SlotsWichOccup = slotsWichOccup;
        m_CompatibleWith = compatibleWith;
        m_Attributes = attr;
        m_ArmorType = arType;


    }
    [SerializeField] ArmorType m_ArmorType;
    public ArmorType ArmorType { get => m_ArmorType; }
    public string Name => m_ItemName;
    public string Description => m_Description;
    public string ConceptName => m_ConceptTypeItem;
    public override List<Object> GetComponents()
    {
        List<Object> components = new List<Object>();
        components.Add(this);
        return components;
    }

    public override string ToString()
    {
        string slotsWichOccup = "";
        m_SlotsWichOccup.ForEach(enu => {
            slotsWichOccup += enu + ", ";
        });

        string compatibleClasses = "";
        m_CompatibleWith.ForEach(enu => {
            compatibleClasses += enu + ", ";
        });

        return base.ToString() + $"Type Armor: {m_ArmorType} \n Slot fill: {m_SlotToFill} \n Slot which occup: {slotsWichOccup} \n Compatible classes: {compatibleClasses} \n {m_Attributes}";
    }

    public Armor getTypeItem()
    {
        return this;
    }
}

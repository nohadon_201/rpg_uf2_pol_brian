using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static EffectMap;

public class Consumable : Item, iItemData<Consumable>
{
    [SerializeField] protected string m_ItemName;
    [SerializeField] protected string m_ConceptName;
    [SerializeField, TextArea] protected string m_Description;
    [SerializeField] protected List<EffectData> m_Effects;
    public string Name => m_ItemName;
    public string ConceptName => m_ConceptName;
    public string Description => m_Description;
    public Consumable getTypeItem()
    {
        return this;
    }
    public void useOn(Character character)
    {
        m_Effects?.ForEach(effect =>
        {
            // effect.ApplayEffect(Character);
            if (effect.timeEffect < 0)
            {
                effectMap.ActiveEffect(effect.effect, effect.valueEffect, effect.buff, character);
            }
            else
            {
                effectMap.PassiveEffect(effect.effect, effect.valueEffect, effect.buff, effect.timeEffect, character, true);
            }
        });
    }
}

public class EffectData
{
    public Effect effect { get; }
    public int valueEffect { get; }
    public int timeEffect { get; }
    public bool buff { get; }
}

public class EffectMap : MonoBehaviour
{
    public static EffectMap effectMap { get; private set; }
    private void Awake()
    {
        if (effectMap != null && effectMap != this)
        {
            Destroy(this);
        }
        else
        {
            effectMap = this;
        }
    }

    public void ActiveEffect(Effect effect, int val, bool buff, Character character)
    {
        switch (effect)
        {
            case Effect.Healing:
                if (buff)
                {
                    character.addHealth(val);
                    break;
                }
                character.addHealth(-val);
                break;
            case Effect.Mana:
                if (buff)
                {
                    character.addMana(val);
                    break;
                }
                character.addMana(-val);
                break;
            case Effect.Experience:
                if (buff)
                {
                    character.addExperience(val);
                    break;
                }
                character.addExperience(-val);
                break;
            case Effect.Stamina:
                if (buff)
                {
                    character.addStamina(val);
                    break;
                }
                character.addStamina(val);
                break;
        }
    }

    public void PassiveEffect(Effect effect, int val, bool buff, int time, Character character, bool startCorutine)
    {
        switch (effect)
        {
            case Effect.MaxHeatlh:
                if (buff)
                {
                    character.incrementMaxHealth(val);
                    break;
                }
                character.decrementMaxHealth(val);
                break;
        }
        if (startCorutine)
            StartCoroutine(TemporaryEffect(effect, val, !buff, time, character));
    }

    private IEnumerator TemporaryEffect(Effect effect, int val, bool buff, int time, Character character)
    {
        yield return new WaitForSeconds(time);
        PassiveEffect(effect, val, buff, 0, character, false);
    }

    public enum Effect
    {
        Healing, Mana, Experience, Stamina, MaxHeatlh, Dexterity, Intelligence, Strength, Vitality, Velocity
    }
}

public interface ConsumableOnCharacter
{
    public void useOn(Character character);
}
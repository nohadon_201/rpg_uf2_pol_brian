

using System.Collections.Generic;
using Unity.VisualScripting.ReorderableList;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

[CreateAssetMenu(fileName = "ItemCreator", menuName = "Item/Item creator", order = 1)]

public class ItemCreator : ScriptableObject
{
    [HideInInspector] public string m_ItemName;
    [HideInInspector] public string m_ConceptTypeItem;
    [HideInInspector] public string m_Description;

    [HideInInspector] public bool m_IsDefensive;
    [HideInInspector] public bool m_IsOffensive;
    [HideInInspector] public bool m_Attributes;
    [HideInInspector] public List<Classes> m_CompatibleWith;

    [HideInInspector] public int m_AtkVal1;
    [HideInInspector] public Element m_AtkElement;
    [HideInInspector] public int m_AtkVal2;

    [HideInInspector] public int m_DeffVal1;
    [HideInInspector] public Element m_DeffElement;
    [HideInInspector] public int m_DeffVal2;

    [HideInInspector] public List<SlotsWeapons> m_SlotsWeaponOccup;
    [HideInInspector] public SlotsWeapons m_SlotsWeaponFill;
    [HideInInspector] public WeaponType m_WeaponType;

    [HideInInspector] public List<SlotsArmor> m_SlotsArmorOccup;
    [HideInInspector] public SlotsArmor m_SlotsArmorFill;
    [HideInInspector] public ArmorType m_ArmorType;

    [HideInInspector] public List<SlotsJewels> m_SlotsJewelsOccup;
    [HideInInspector] public SlotsJewels m_SlotsJewelsFill;
    [HideInInspector] public JewelType m_JewelType;

    [HideInInspector] public SlotType m_SlotType;
    [HideInInspector] public TypeItem TypeItem;

    [HideInInspector] public Item checkOut;
}
public enum SlotType { Weapon, Armor, Jewel }
public enum TypeItem { Equipment, Consumable }

//CanEditMultipleObjects
//True
//[CustomEditor(typeof(Weapon))]


[CustomEditor(typeof(ItemCreator))]
public class Item_Editor : Editor
{

    MyAttributes m_MyAttributes;

    private void OnEnable()
    {
        m_MyAttributes = new MyAttributes();
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        //DrawDefaultInspector();

        ItemCreator myTarget = (ItemCreator)target;
        SerializedObject so = new SerializedObject(myTarget);
        SerializedProperty classList = so.FindProperty("m_CompatibleWith");
        SerializedProperty wpnList = so.FindProperty("m_SlotsWeaponOccup");
        SerializedProperty armList = so.FindProperty("m_SlotsArmorOccup");
        SerializedProperty jwlList = so.FindProperty("m_SlotsJewelsOccup");
        SerializedProperty checkout = so.FindProperty("checkOut");

        EditorGUILayout.Space(4);
        myTarget.m_ItemName = EditorGUILayout.TextField("Field name:", myTarget.m_ItemName);

        EditorGUILayout.Space(4);
        myTarget.m_ConceptTypeItem = EditorGUILayout.TextField("Concept type item:", myTarget.m_ConceptTypeItem);

        EditorGUILayout.Space(4);
        EditorGUILayout.LabelField("Item description");
        myTarget.m_Description = EditorGUILayout.TextArea(myTarget.m_Description, GUILayout.Height(40));
        EditorGUILayout.Space(4);

        EditorGUILayout.LabelField("Type of item", EditorStyles.boldLabel);
        myTarget.TypeItem = (TypeItem)EditorGUILayout.EnumPopup("", myTarget.TypeItem);
        EditorGUILayout.Space(4);

        if (myTarget.TypeItem == TypeItem.Equipment)
        {

            myTarget.m_SlotType = (SlotType)EditorGUILayout.EnumPopup("Type:", myTarget.m_SlotType);


            switch (myTarget.m_SlotType)
            {
                case SlotType.Weapon:
                    myTarget.m_SlotsWeaponFill = (SlotsWeapons)EditorGUILayout.EnumPopup("Slot Weapon:", myTarget.m_SlotsWeaponFill);
                    myTarget.m_WeaponType = (WeaponType)EditorGUILayout.EnumPopup("Weapon Type:", myTarget.m_WeaponType);
                    EditorGUILayout.Space(8);
                    ReorderableListGUI.Title("Slot/s occup");
                    ReorderableListGUI.ListField(wpnList);

                    break;
                case SlotType.Armor:
                    myTarget.m_SlotsArmorFill = (SlotsArmor)EditorGUILayout.EnumPopup("Slot Armor:", myTarget.m_SlotsArmorFill);
                    myTarget.m_ArmorType = (ArmorType)EditorGUILayout.EnumPopup("Armor Type:", myTarget.m_ArmorType);
                    EditorGUILayout.Space(8);
                    ReorderableListGUI.Title("Slot/s occup");
                    ReorderableListGUI.ListField(armList);

                    break;
                case SlotType.Jewel:
                    myTarget.m_SlotsJewelsFill = (SlotsJewels)EditorGUILayout.EnumPopup("Slot Jewel:", myTarget.m_SlotsJewelsFill);
                    myTarget.m_JewelType = (JewelType)EditorGUILayout.EnumPopup("Jewel Type:", myTarget.m_JewelType);
                    EditorGUILayout.Space(8);
                    ReorderableListGUI.Title("Slot/s occup");
                    ReorderableListGUI.ListField(jwlList);
                    break;
            }

            ReorderableListGUI.Title("Compatible classes");
            ReorderableListGUI.ListField(classList);

            EditorGUILayout.Space(8);

            myTarget.m_Attributes = EditorGUILayout.Toggle("Have attributes", myTarget.m_Attributes);

            if (myTarget.m_Attributes)
            {
                m_MyAttributes.Dexterity = EditorGUILayout.IntSlider("Dexterity", (int)m_MyAttributes.Dexterity, 0, 100);
                m_MyAttributes.Intelligence = EditorGUILayout.IntSlider("Intelligence", (int)m_MyAttributes.Intelligence, 0, 100);
                m_MyAttributes.Strength = EditorGUILayout.IntSlider("Strength", (int)m_MyAttributes.Strength, 0, 100);
                m_MyAttributes.Vitality = EditorGUILayout.IntSlider("Vitality", (int)m_MyAttributes.Vitality, 0, 100);
                m_MyAttributes.Velocity = EditorGUILayout.IntSlider("Velocity", (int)m_MyAttributes.Velocity, 0, 100);
                EditorGUILayout.Space(8);
            }
            else
            {
                m_MyAttributes = new MyAttributes();
            }

            myTarget.m_IsOffensive = EditorGUILayout.Toggle("Is defensive", myTarget.m_IsOffensive);

            if (myTarget.m_IsOffensive)
            {
                myTarget.m_AtkElement = (Element)EditorGUILayout.EnumPopup("Type of element:", myTarget.m_AtkElement);
                myTarget.m_AtkVal1 = EditorGUILayout.IntField("Deff value", myTarget.m_AtkVal1);
                myTarget.m_AtkVal2 = EditorGUILayout.IntField("Deff element value", myTarget.m_AtkVal2);
                EditorGUILayout.Space(8);
            }
            else
            {
                myTarget.m_AtkElement = Element.NONE;
                myTarget.m_AtkVal1 = 0;
                myTarget.m_AtkVal2 = 0;
            }

            myTarget.m_IsDefensive = EditorGUILayout.Toggle("Is offensive", myTarget.m_IsDefensive);

            if (myTarget.m_IsDefensive)
            {
                myTarget.m_DeffElement = (Element)EditorGUILayout.EnumPopup("Type of element:", myTarget.m_DeffElement);
                myTarget.m_DeffVal1 = EditorGUILayout.IntField("Atk value", myTarget.m_DeffVal1);
                myTarget.m_DeffVal2 = EditorGUILayout.IntField("Atk element value", myTarget.m_DeffVal2);
                EditorGUILayout.Space(8);
            }
            else
            {
                myTarget.m_DeffElement = Element.NONE;
                myTarget.m_DeffVal1 = 0;
                myTarget.m_DeffVal2 = 0;
            }

            EditorGUILayout.Space(8);

            if (GUILayout.Button("Create"))
            {
                Equipment baseEquipment = null;
                string path = "Assets/Items/Equipment";
                switch (myTarget.m_SlotType)
                {
                    case SlotType.Weapon:
                        Weapon wp = new Weapon(myTarget.m_SlotsWeaponFill, myTarget.m_SlotsWeaponOccup, myTarget.m_CompatibleWith, m_MyAttributes, myTarget.m_WeaponType);
                        wp.setInfoItem(myTarget.m_ItemName, myTarget.m_ConceptTypeItem, myTarget.m_Description);
                        baseEquipment = wp;
                        path += "/Weapons";
                        break;
                    case SlotType.Armor:
                        Armor ar = new Armor(myTarget.m_SlotsArmorFill, myTarget.m_SlotsArmorOccup, myTarget.m_CompatibleWith, m_MyAttributes, myTarget.m_ArmorType);
                        ar.setInfoItem(myTarget.m_ItemName, myTarget.m_ConceptTypeItem, myTarget.m_Description);
                        baseEquipment = ar;
                        path += "/Armors";
                        break;
                    case SlotType.Jewel:
                        Jewel jw = new Jewel(myTarget.m_SlotsJewelsFill, myTarget.m_SlotsJewelsOccup, myTarget.m_CompatibleWith, m_MyAttributes, myTarget.m_JewelType);
                        jw.setInfoItem(myTarget.m_ItemName, myTarget.m_ConceptTypeItem, myTarget.m_Description);
                        baseEquipment = jw;
                        path += "/Jewels";
                        break;
                }
                
                path += "/" + myTarget.m_ItemName + ".asset";

                if (myTarget.m_IsDefensive)
                {
                    baseEquipment = new Defensive(baseEquipment, myTarget.m_DeffVal1, myTarget.m_DeffElement, myTarget.m_DeffVal2);
                }

                if (myTarget.m_IsOffensive)
                {
                    baseEquipment = new Offensive(baseEquipment, myTarget.m_AtkVal1, myTarget.m_AtkElement, myTarget.m_AtkVal2);
                }

                AssetDatabase.CreateAsset(baseEquipment, path);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = baseEquipment;
            }
        }

        EditorGUILayout.Space(4);
        EditorGUILayout.PropertyField(checkout);

        if (GUILayout.Button("Check item"))
        {
            if (myTarget.checkOut is Equipment)
            {
                Equipment a = (Equipment)myTarget.checkOut;

                a.GetComponents().ForEach(comp => {
                    Debug.Log(comp);
                });
            }
        }

        so.ApplyModifiedProperties();
    }
}

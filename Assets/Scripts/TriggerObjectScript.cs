using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using static Player;

public class TriggerObjectScript : MonoBehaviour
{
    [SerializeField] public TriggerObject triggerObject;
    void Start()
    {
        if (!triggerObject.Triggered)
        {
            this.gameObject.SetActive(true);
        }
        else
        {
            this.gameObject.SetActive(false);
        }
        
    }
    public void DesactivateSelf()
    {
        if(!triggerObject.Triggered)
        {
            triggerObject.Triggered = true;
            this.gameObject.SetActive(false);
        }
    }
    public IEnumerator showtext(string v,Color c)
    {
        TextMeshProUGUI text = this.GetComponent<TextMeshProUGUI>();
        text.color = c;
        text.text = v;
        
        while (text.color.a > 0)
        {
            yield return new WaitForSeconds(0.2f);
            text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - 0.1f);
        }

    }

}
public interface TriggerObjectInterface
{
    public abstract void triggerIn();
    public abstract void triggerOut();

}

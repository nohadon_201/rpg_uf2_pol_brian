using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        if (GameManagerControler.PartidaGuardada())
        {
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(1).gameObject.SetActive(false);
        }
        else
        {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void carrgarParitda()
    {
        SceneManager.LoadScene(GameManagerControler.getLastScene());    
    }
    public void StartGame()
    {
        if (GameManagerControler.PartidaGuardada())
        {

        }
        SceneManager.LoadScene("WorldScene");
    }
    public void Exit()
    {
        UnityEditor.EditorApplication.isPlaying = false;
    }
}

using OdinSerializer;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameManagerControler: MonoBehaviour
{
    public SaveDataSO dataSO;
    public void Awake()
    {
        if (PartidaGuardada())
        {
            this.dataSO = LoadData();
            GameObject poolTriggers = GameObject.Find("TriggerObjects");
            print(dataSO.triggers.Count);
            for (int a = 0; a < poolTriggers.transform.childCount; a++)
            {
                poolTriggers.transform.GetChild(a).GetComponent<TriggerObjectScript>().triggerObject = dataSO.triggers[a];
                print(dataSO.triggers[a].Triggered);
            }
        }
    }
    void Start()
    {
        if (PartidaGuardada())
        {
            Player.Instance.transform.position=Player.Instance.Data.transform;
            GameObject cam = GameObject.Find("Main Camera");
            cam.transform.position.Set(Player.Instance.Data.transform.x, Player.Instance.Data.transform.y, cam.transform.position.z);
            
        }
        else
        {
            Player.Instance.UpdatePosition();
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F5))
        {
            SaveData();
        }
    }
    public static bool PartidaGuardada()
    {
        return File.Exists("SaveDataGame.json");
    }
    public static string getLastScene()
    {
        return LoadData().nameScene;
    }
    public void SaveData()
    {
        dataSO.UpdateBeforeSave();
        byte[] serializedData = SerializationUtility.SerializeValue<SaveDataSO>(dataSO, DataFormat.JSON);
        string base64 = System.Convert.ToBase64String(serializedData);
        File.WriteAllText( "SaveDataGame.json", base64);
    }
    public static SaveDataSO LoadData()
    {
        string newBase64 = File.ReadAllText("SaveDataGame.json");
        byte[] postFile = System.Convert.FromBase64String(newBase64);
       return SerializationUtility.DeserializeValue<SaveDataSO>(postFile, DataFormat.JSON);
    }
}

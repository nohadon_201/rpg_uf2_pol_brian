using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField]
    private GameObject Player;
    private Transform Transform;
    void Awake()
    {
        if(Player != null)
        {
            Transform= Player.transform;    
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 finalPoint = new Vector3(Transform.position.x, Transform.position.y, this.transform.position.z);
        transform.position = Vector3.Lerp(transform.position, finalPoint, Time.deltaTime * 3);
    }
}

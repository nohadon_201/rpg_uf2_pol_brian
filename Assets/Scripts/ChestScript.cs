using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UIElements;

public class ChestScript : TriggerObjectScript,TriggerObjectInterface 
{
    [SerializeField]
    private List<Items> ItemPut;
    [SerializeField]
    private bool key;
    [SerializeField]
    private bool coins;
    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void getChestLoot()
    {
        print("aaaa");
        if (ItemPut != null && ItemPut.Count!=0)
        {
            print("aaa");
            foreach(Items i in ItemPut){
                Player.Instance.Data.addObjectToInventary(i);
            }
            
        }
        else if(key)
        {
            Player.Instance.Data.sumKey();
        }
        else if (coins)
        {
            print("Monedes jugador abans: " + Player.Instance.Data.getCoins());
            Player.Instance.Data.sumCoins((int)Random.Range(3, 10));
            print("Monedes jugador despres: " + Player.Instance.Data.getCoins());
        }
        this.DesactivateSelf();
    }


    public void triggerIn()
    {
        Player.Instance.interact += getChestLoot;
    }
    public void triggerOut()
    {
        Player.Instance.interact -= getChestLoot;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
[CreateAssetMenu]
public class SaveDataSO : ScriptableObject
{
    public PlayerData dataPlayer;
    public List<TriggerObject> triggers;
    public string nameScene;
    public void UpdateBeforeSave()
    {
        this.nameScene=SceneManager.GetActiveScene().name;
        Player.Instance.UpdatePosition();
    }

}

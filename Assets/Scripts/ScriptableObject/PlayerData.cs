using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerData : ScriptableObject
{
    [Space(3)]
    [SerializeField] private List<Items> items = new List<Items>(); 
    [SerializeField] private List<Character> Team = new List<Character>();
    [SerializeField] private Vector3 transform_player;
    [SerializeField] private int numKeys;
    [SerializeField]private int Coins;

    public void addObjectToInventary(Items i)
    {
        this.items.Add(i);
    }
    public Items AccessToObject(int ind)
    {
        return items[ind];
    }
    public List<Consumible> returnConsumibles()
    {
        List<Consumible> list = new List<Consumible>();
        foreach (var item in this.items)
        {
            if(item is Consumible)
            {
                list.Add((Consumible)item);
            }
        }
        return list;
    }
    public bool hasKey()
    {
        if (this.numKeys > 0)
        {
            numKeys--;
            return true;    
        }
        return false;
    }

    public void sumKey()
    {
        this.numKeys++;
    }
    public void sumCoins(int a)
    {
        this.Coins+= a; 
    }
    public int getCoins()
    {
        return this.Coins;
    }
    public int getKeys()
    {
        return numKeys;
    }

    public List<Character> team { get => Team; private set => Team = value; }
    public Vector3 transform { get => transform_player; set => transform_player = value; }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class DoorScript : TriggerObjectScript, TriggerObjectInterface
{
    void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void getDoorOpen()
    {
        if (Player.Instance.Data.hasKey())
        {
            this.DesactivateSelf();
        }
    }

    public void triggerIn()
    {
        Player.Instance.interact += getDoorOpen;
    }
    public void triggerOut()
    {
        Player.Instance.interact -= getDoorOpen;
    }
}


using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    
    [SerializeField] private PlayerData data;
    public PlayerData Data { get => data; private set => data = value; }

    public delegate void PlayerInteractEventHandler();
    public PlayerInteractEventHandler interact;
    public static Player Instance { get; private set; }
    private void Awake()
    {   
        DontDestroyOnLoad(this.gameObject);
        // If there is an instance, and it's not me, delete myself.
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            interact?.Invoke();
        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "CombatTrigger")
        {
            GetComponent<PlayerInput>().enabled = false;
            //GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            SceneManager.LoadScene("Combat");
        }
        if (collision.gameObject.tag == "Cofre")
        {
            GameObject TriggerObject = collision.transform.parent.gameObject;
            TriggerObject.GetComponent<ChestScript>().triggerIn();
        }else if(collision.gameObject.tag == "Puerta")
        {

            GameObject TriggerObject = collision.transform.parent.gameObject;
            TriggerObject.GetComponent<DoorScript>().triggerIn();
        }
    }
    
    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Cofre")
        {
            GameObject TriggerObject = collision.transform.parent.gameObject;
            TriggerObject.GetComponent<ChestScript>().triggerOut();
        }else if(collision.gameObject.tag == "Puerta")
        {
            GameObject TriggerObject = collision.transform.parent.gameObject;
            TriggerObject.GetComponent<DoorScript>().triggerOut();

        }
    }
    
    public void Movement(InputAction.CallbackContext context)
    {
        GetComponent<Rigidbody2D>().velocity =(context.ReadValue<Vector2>() * Clamp(100000000,2,10));

    }
    public void UpdatePosition()
    {
        this.data.transform = this.transform.position;
    }
    public static int Clamp(int value, int min, int max)
    {
        return (value < min) ? min : (value > max) ? max : value;
    }
}
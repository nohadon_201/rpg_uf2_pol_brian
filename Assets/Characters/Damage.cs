using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage {
    public Damage(int Damage, Element element) {
        damage = Damage;
        Element = element;
    }

    public int damage = 0;
    public Element Element = Element.NONE;
}

public interface IDoDamage
{
    public void doDamageTo(IDamagable damaged);
}

public interface IDamagable
{
    public void getDamaged(Damage data);
}

using OdinSerializer.Utilities;
using System;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime.Collections;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;

[CreateAssetMenu(fileName = "Character", menuName = "New Character", order = 1)]
public class Character : ScriptableObject, IDoDamage, IDamagable
{
    //Personal-Data
    [Space(5)]
    [SerializeField, Header("Personal Data")] private string m_CharacterName;
    [SerializeField] Classes m_CharacterClass;
    Class m_Class;
    public Class Class { get => m_Class; }
    public Classes ClassType { get => m_CharacterClass; }

    //Attributes
    int m_Experience = 0;
    int m_MaxExperience = 100;
    public int Experience { get => m_Experience; }
    public int MaxExperience { get => m_MaxExperience; }
    public void addExperience(int xp)
    {
        if (m_Experience + xp >= m_MaxExperience)
        {
            this.lvlUp();
            m_Experience += m_Experience + xp - m_MaxExperience;
            return;
        }
        m_Experience += xp;
    }
    
    int m_Heatlh;
    int m_MaxHeatlh;
    public int Heatlh { get => m_Heatlh; }
    public int MaxHeatlh { get => m_MaxHeatlh; }
    public void addHealth(int hp) {
        if (hp + m_Heatlh > m_MaxHeatlh) {
            m_Heatlh = m_MaxHeatlh;
            return;
        }
        m_Heatlh += hp;
    }

    /// <summary>
    /// Incrementar� la vida maxima del personaje, este tendra en cuenta el porcentaje de la vida anterior y se actualizara la nueva vida.
    /// </summary>
    /// <param name="value"></param>
    public void incrementMaxHealth(int value) {
        int percentHp = m_Heatlh * 100 / m_MaxHeatlh;
        m_MaxHeatlh += value;
        int NewHp = m_MaxHeatlh * percentHp / 100;
        m_Heatlh = NewHp;
    }
    /// <summary>
    /// Decrementara la vida maxima del personaje, si la vida actual sobrepasa la maxima, la reducir�.
    /// </summary>
    /// <param name="value"></param>
    public void decrementMaxHealth(int value)
    {
        m_MaxHeatlh -= value;
        if (m_Heatlh > m_MaxHeatlh) {
            m_Heatlh = m_MaxHeatlh;
        }
    }

    int m_Mana;
    int m_MaxMana;
    public int Mana { get => m_Mana; }
    public int MaxMana { get => m_MaxMana; }
    public void addMana(int mana) {
        if (mana + m_Mana > m_MaxMana)
        {
            m_Heatlh = m_MaxMana;
            return;
        }
        m_Mana += mana;
    }

    int m_Stamina;
    int m_MaxStamina;
    public int Stamina { get => m_Stamina; }
    public int MaxStamina { get => m_MaxStamina; }
    public void addStamina(int stamina) {
        if (stamina + m_Stamina > m_MaxStamina)
        {
            m_Stamina = m_MaxStamina;
            return;
        }
        m_Stamina += stamina;
    }

    [Space(10)]
    [SerializeField, Range(1, 60), Header("Attributes"), Space(2)] private int m_Level;
    public int Level { get => m_Level; private set => m_Level = value; }
    [SerializeField] MyAttributes m_Attributes;

    MyAttributes m_TotalAttributes;
    public MyAttributes TotalAttributes { get => m_TotalAttributes; }

    //Equipment
    Dictionary<SlotsWeapons, Equipment> m_WeaponSlots = new Dictionary<SlotsWeapons, Equipment>();
    Dictionary<SlotsJewels, Equipment> m_JewerlSlots = new Dictionary<SlotsJewels, Equipment>();
    Dictionary<SlotsArmor, Equipment> m_ArmorSlots = new Dictionary<SlotsArmor, Equipment>();

    /// <summary>
    /// Sumar� los atributos totales de los objetos junto con los atributos base del personaje y los asignara a una nueva tabla "m_TotalAttributes" .
    /// </summary>
    private void updateTotalAttributes()
    {
        MyAttributes totalAttr = new MyAttributes();
        totalAttr.addAttributes(m_Attributes);

        m_WeaponSlots.ForEach(entry =>
        {
            entry.Value?.GetComponents().ForEach(component =>
            {
                if (component is Weapon)
                {
                    Weapon equipment = (Weapon)component;
                    totalAttr.addAttributes(equipment.Attributes);
                }
            });
        });

        m_JewerlSlots.ForEach(entry =>
        {
            entry.Value?.GetComponents().ForEach(component =>
            {
                if (component is Jewel)
                {
                    Jewel equipment = (Jewel)component;
                    totalAttr.addAttributes(equipment.Attributes);
                }
            });
        });

        m_ArmorSlots.ForEach(entry =>
        {
            entry.Value?.GetComponents().ForEach(component =>
            {
                if (component is Armor)
                {
                    Armor equipment = (Armor)component;
                    totalAttr.addAttributes(equipment.Attributes);
                }
            });
        });

        m_TotalAttributes = totalAttr;
    }

    /// <summary>
    /// Comprobar� si el equipo que obtiene es compatible con la clase del personaje.
    /// </summary>
    /// <param name="equipment"></param>
    /// <returns>boolean</returns>

    public bool isCompatible(Equipment equipment)
    {
        bool condition = false;

        equipment.GetComponents().ForEach(comp =>
        {
            if (comp is Armor)
                if (((Armor)comp).CompatibleWith.Contains(this.m_CharacterClass))
                {
                    condition = true;
                    return;
                }


            if (comp is Weapon)
                if (((Weapon)comp).CompatibleWith.Contains(this.m_CharacterClass))
                {
                    condition = true;
                    return;
                }

            if (comp is Jewel)
                if (((Jewel)comp).CompatibleWith.Contains(this.m_CharacterClass))
                {
                    condition = true;
                    return;
                }
        });

        return condition;
    }

    /// <summary>
    /// Equipar� el equipamiento en su ranura correspondiente, si hab�a un equipo asignado en esa ranura, veolvera el o los equipamientos que ocupan el objeto.
    /// </summary>
    /// <param name="equipment"></param>
    /// <returns>An empty list or a list with equipment/s</returns>

    public List<Equipment> Equip(Equipment equipment)
    {
        List<Equipment> myEquipment = null;
        equipment.GetComponents().ForEach(comp =>
        {
            if (comp is Weapon)
            {
                Weapon NewWeapon = (Weapon)comp;
                m_WeaponSlots.ForEach(entry =>
                {
                    bool isOccuping = false;
                    Weapon weaponEquipped = (Weapon)entry.Value;
                    weaponEquipped?.SlotsWichOccup.ForEach(slotA =>
                    {
                        if (NewWeapon.SlotsWichOccup.Contains(slotA))
                        {
                            isOccuping = true;
                        }
                    });

                    if (isOccuping)
                    {
                        myEquipment.Add(weaponEquipped);
                        m_WeaponSlots.Add(entry.Key, null);
                    }
                });
                m_WeaponSlots.Add(NewWeapon.SlotToFill, NewWeapon);
            }

            if (comp is Jewel)
            {
                Jewel NewJewel = (Jewel)comp;
                m_JewerlSlots.ForEach(entry =>
                {
                    bool isOccuping = false;
                    Jewel jewelEquipped = (Jewel)entry.Value;
                    jewelEquipped?.SlotsWichOccup.ForEach(slotA =>
                    {
                        if (NewJewel.SlotsWichOccup.Contains(slotA))
                        {
                            isOccuping = true;
                        }
                    });

                    if (isOccuping)
                    {
                        myEquipment.Add(jewelEquipped);
                        m_JewerlSlots.Add(entry.Key, null);
                    }
                });
                m_JewerlSlots.Add(NewJewel.SlotToFill, NewJewel);
            }

            if (comp is Armor)
            {
                Armor NewArmor = (Armor)comp;
                //Primero buscaremos por cada equipamiento que tenemos en el diccionario de armaduras
                m_ArmorSlots.ForEach(entry =>
                {
                    bool isOccuping = false;
                    Armor armorEquipped = (Armor)entry.Value;
                    //Comprobamos si la armadura equipada est� ocupando el slot donde quiere entrar la nueva armadura.
                    armorEquipped?.SlotsWichOccup.ForEach(slotA =>
                    {
                        if (NewArmor.SlotsWichOccup.Contains(slotA))
                        {
                            isOccuping = true;
                        }
                    });

                    //Si la nueva armadura ocupa un slot de alg�n equipamiento actual, este se a�adira en la lista y vaciara el diccionario.
                    if (isOccuping)
                    {
                        myEquipment.Add(armorEquipped);
                        m_ArmorSlots.Add(entry.Key, null);
                    }
                });
                m_ArmorSlots.Add(NewArmor.SlotToFill, NewArmor);
            }
        });
        updateTotalAttributes();
        return myEquipment;
    }

    public void lvlUp()
    {
        if (m_Class == null)
            return;
        m_Level++;
        float[] attrDef = m_Class.attributesDefinition();
        m_Attributes.Dexterity = attrDef[0] * m_Level;
        m_Attributes.Intelligence = attrDef[1] * m_Level;
        m_Attributes.Strength = attrDef[2] * m_Level;
        m_Attributes.Vitality = attrDef[3] * m_Level;
        m_Attributes.Velocity = attrDef[4] * m_Level;

        // ToDo
        m_MaxHeatlh = Mathf.FloorToInt(m_Attributes.Vitality * m_Level * 10);
        m_Heatlh = m_MaxHeatlh;
        m_MaxExperience = m_Level * 100;
    }

    

    public void doDamageTo(IDamagable damaged)
    {
        //ToDo
        
        HashSet<Element> elements = new HashSet<Element>();
        int totalDmg = 0;
        foreach(KeyValuePair<SlotsWeapons, Equipment> entry in m_WeaponSlots)
        {
            entry.Value?.GetComponents().ForEach(myObject => { 
                if(myObject is Offensive)
                {
                    Offensive o = (Offensive) myObject;
                    totalDmg += o.Val;
                    elements.Add(o.Element);
                                
                }
            });
        }
        totalDmg += (TotalAttributes.getStrength * m_Level);
        if (elements.Count > 1)
        {

        }
        else if(elements.Count == 0)
        {
            Damage dmg = new Damage(totalDmg, Element.NONE);
            damaged.getDamaged(dmg);
        }
        else
        {
            //Damage dmg = new Damage(totalDmg, elements.);
            //damaged.getDamaged(dmg);
        }
        
    }

    public void getDamaged(Damage data)
    {
        //ToDo
    }
}

public abstract class Skill
{

}
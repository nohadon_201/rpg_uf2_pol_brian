using System;
using UnityEngine;

[Serializable]
public class MyAttributes : ScriptableObject
{
    [Range(0, 100)] public float Dexterity;
    [Range(0, 100)] public float Intelligence;
    [Range(0, 100)] public float Strength;
    [Range(0, 100)] public float Vitality;
    [Range(0, 100)] public float Velocity;

    public int getDexterity { get => Mathf.FloorToInt(Dexterity); }
    public int getIntelligence { get => Mathf.FloorToInt(Intelligence); }
    public int getStrength { get => Mathf.FloorToInt(Strength); }
    public int getVitality { get => Mathf.FloorToInt(Vitality); }
    public int getVelocity { get => Mathf.FloorToInt(Velocity); }

    public override string ToString()
    {
        return $"Dexterity: {getDexterity} \n Intelligence: {getIntelligence} \n Strength: {getStrength} \n Vitality: {getVitality} \n Velocity: {getVelocity}";
    }

    public void addAttributes(MyAttributes attributes) {
        Dexterity += attributes.getDexterity;
        Intelligence += attributes.getIntelligence;
        Strength += attributes.getStrength;
        Vitality += attributes.getVitality;
        Velocity += attributes.getVelocity;
    }
}